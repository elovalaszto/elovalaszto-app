        <section class="section">
          <div class="section-header">
            <h1>PIN kérése</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->

            <div class="card">
              <div class="card-body" style="margin: 0 auto;">
		<span class="btn btn-icon icon-right btn-success no-event margin-right-15 under-500 under-500-marginbottom-15">1. lépés <i class="fas fa-check-circle"></i></span>
		<span class="btn btn-icon icon-right btn-success no-event margin-right-15 under-500 under-500-marginbottom-15">2. lépés <i class="fas fa-check-circle"></i></span>
		<span class="btn btn-primary no-event under-500">3. lépés <i class="fas fa-edit"></i></span>
              </div>
            </div>

            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php echo $this->Form->create($user); ?>
              	<?php echo $this->Form->control('password', ['label' => __('Jelszó'), 'class' => 'form-control', 'required', 'autofocus']); ?>
              	<div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->submit(__('Tovább'), ['class' => 'btn btn-outline-primary']); ?>
              	<?php echo $this->Form->end(); ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>

        </section>